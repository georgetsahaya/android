package com.gts.mycba;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.google.gson.Gson;
import com.gts.mycba.model.CbaTxn;
import com.gts.mycba.model.TxnData;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

import static com.gts.mycba.CbaConstants.DATE_FORMAT;
import static com.gts.mycba.CbaConstants.DATE_HEADER;
import static com.gts.mycba.CbaConstants.JSONCONTENTFILE;
import static com.gts.mycba.CbaConstants.MSEC_IN_A_DAY;
import static com.gts.mycba.CbaConstants.PENDING_PREFIX;

public class MainActivity extends AppCompatActivity {
    RecyclerView txnRecycleView;
    TxnRecyclerAdapter txnAdapter;

    TxnData txnData;
    LinkedList<CbaTxn> txns = new LinkedList<CbaTxn>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ActionBar bar = getSupportActionBar();
        bar.setIcon(R.drawable.icon_action_logo);
        bar.setTitle(getResources().getString(R.string.app_main_title));
        bar.setDisplayShowHomeEnabled(true);

        invalidateOptionsMenu();

        txnRecycleView = findViewById(R.id.recycler_view);

        new RetrieveJsonTask().execute(CbaConstants.URL);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.show_expense:
                showFortnightExpenseAverage(calculateFortnightExpense());
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private float calculateFortnightExpense() {
        Map<Integer, Float> expenseTrackerMap = new HashMap<>();
        if (txns != null && txns.size() > 1) {
            for (int i = 1;i < txns.size();i++) {
                CbaTxn cbaTxn = txns.get(i);

                if (cbaTxn != null && cbaTxn.effectiveDate != null
                        && cbaTxn.effectiveDate.length() > 0  //if date is available
                        && cbaTxn.amount < 0) {  //negative values to track the expenses
                    Date date = getDate(cbaTxn.effectiveDate);
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(date);
                    int weekOfYear = calendar.get(Calendar.WEEK_OF_YEAR);
                    //Make it even so that 27 fortnights are counted
                    int fortnightOfYear = (weekOfYear + (weekOfYear % 2)) / 2;
                    Float expense = expenseTrackerMap.get(fortnightOfYear);
                    if (expense == null) {
                        expenseTrackerMap.put(fortnightOfYear, cbaTxn.amount);
                    } else {
                        expenseTrackerMap.put(fortnightOfYear, expense + cbaTxn.amount);
                    }
                }
            }
        }

        if (!expenseTrackerMap.isEmpty()) {
            Set<Map.Entry<Integer, Float>> entries = expenseTrackerMap.entrySet();
            Float total = 0f;
            for (Map.Entry<Integer, Float> entry:entries) {
                total += entry.getValue();
            }

            return total / (float) entries.size();
        }

        return 0;
    }

    private void showFortnightExpenseAverage(float expenses) {
        if (expenses == 0) {
            return; //Ignore silently
        }

        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
        alertDialog.setTitle(R.string.fortnight_expense_title);
        alertDialog.setMessage(getString(R.string.fortnight_expense_text, Math.abs(expenses)));
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    class RetrieveJsonTask extends AsyncTask<String, Void, Boolean> {
        protected Boolean doInBackground(String... urls) {
            try {
                parseJsonFile(urls[0]);

                return txnData != null && txns.size() > 0;
            } catch (Exception e) {
                return false;
            }
        }

        protected void onPostExecute(Boolean isOk) {
            if (isOk && MainActivity.this != null && !MainActivity.this.isFinishing() && !MainActivity.this.isDestroyed()) {
                txnAdapter = new TxnRecyclerAdapter(getApplicationContext(), txnData, txns);
                final RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(MainActivity.this);
                txnRecycleView.setLayoutManager(mLayoutManager);
                txnRecycleView.setItemAnimator(new DefaultItemAnimator());
                txnRecycleView.setAdapter(txnAdapter);
            } else {
            }
        }
    }

    private Date getDate(String str) {
        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);
        try {
            Date date = format.parse(str);
            return date ;
        } catch (Exception e) {
            return null ;
        }
    }

    private void parseJsonFile(String urlString) {
        HttpURLConnection connection = null;
        BufferedReader bReader = null;
        InputStream inputStream = null;

        try {
            try {
                URL url = new URL(urlString);
                connection = (HttpURLConnection) url.openConnection();
                inputStream = connection.getInputStream();
            } catch (IOException ioe) {
                inputStream = getApplicationContext().getAssets().open(JSONCONTENTFILE);
            }
            bReader = new BufferedReader(new InputStreamReader(inputStream));

            StringBuilder sb = new StringBuilder();
            String line ;
            while ((line = bReader.readLine()) != null){
                sb.append(line);
            }
            Gson gson = new Gson();

            txnData = gson.fromJson(sb.toString(), TxnData.class) ;

            txns.addAll(txnData.transactions) ;

            for (int i = 0;i < txnData.pending.size();i++) {
                CbaTxn txn = txnData.pending.get(i);
                txn.description = PENDING_PREFIX + txn.description;
                txns.add(txn);
            }

            Collections.sort(txns, new Comparator<CbaTxn>() {
                public int compare(CbaTxn o1, CbaTxn o2) {
                    Date d1 = getDate(o1.effectiveDate) ;
                    Date d2 = getDate(o2.effectiveDate) ;

                    return d2.compareTo(d1);
                }
            });

            txns.add(0, new CbaTxn());

            addDateField(txns);
            bReader.close();
            inputStream.close();
        } catch(Exception e) {
            e.printStackTrace() ;
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    public String getDateDifferenceFlag(String dateArg) {
        String toRet = "";

        Date today = new Date();
        Date date = getDate(dateArg);

        int difference = today.compareTo(date);

        int diffInDays = (int) ((today.getTime() - date.getTime())/ MSEC_IN_A_DAY);

        if (difference == 0)
            return getString(R.string.today);
        else
            return getString(R.string.days_ago, diffInDays) ;
    }

    private void addDateField(LinkedList list) {
        CbaTxn txn = new CbaTxn();
        txn.id = DATE_HEADER;
        txn.effectiveDate = ((CbaTxn)list.get(1)).effectiveDate;
        txn.description = getDateDifferenceFlag(txn.effectiveDate);

        list.add(1, txn);
        int j = 2 ;

        for (int i = 3;i < list.size();i++) { //First two items are summary and the first date header - hence ignore them
            CbaTxn txn1 = (CbaTxn) list.get(j);
            CbaTxn txn2 = (CbaTxn) list.get(i);

            if (!txn1.effectiveDate.equals(txn2.effectiveDate)) {
                CbaTxn txn3 = new CbaTxn();
                txn3.id = DATE_HEADER;
                txn3.effectiveDate = txn2.effectiveDate;
                txn3.description = getDateDifferenceFlag(txn3.effectiveDate);

                list.add(i, txn3);

                j = i ;

            }
        }
    }
}
