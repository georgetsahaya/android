package com.gts.mycba.model;

import java.io.Serializable;

public class TxnAtmData implements Serializable {
    public String id;
    public String name;
    public String address;
    public TxnAtmLocation location;
}
