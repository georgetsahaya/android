package com.gts.mycba.model;

import com.gts.mycba.model.CbaTxn;
import com.gts.mycba.model.TxnAccountSummary;
import com.gts.mycba.model.TxnAtmData;

import java.util.ArrayList;

public class TxnData {
    public TxnAccountSummary account;
    public ArrayList<CbaTxn> transactions;
    public ArrayList<CbaTxn> pending;
    public ArrayList<TxnAtmData> atms;
}
