package com.gts.mycba.model;

import java.io.Serializable;

public class TxnAtmLocation implements Serializable {
    public String lat;
    public String lng;
}
