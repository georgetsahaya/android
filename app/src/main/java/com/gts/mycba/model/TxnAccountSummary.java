package com.gts.mycba.model;

import java.io.Serializable;

public class TxnAccountSummary implements Serializable {
    public String accountName;
    public String accountNumber;
    public float available;
    public float balance;
}
