package com.gts.mycba.model;

import java.io.Serializable;

public class CbaTxn implements Serializable {
    public String id;
    public String effectiveDate;
    public String description;
    public float amount;
    public String atmId;
}
