package com.gts.mycba;

import android.content.Context;
import android.support.annotation.IntDef;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gts.mycba.holder.TxnAtmViewHolder;
import com.gts.mycba.holder.TxnDateViewHolder;
import com.gts.mycba.holder.TxnHeaderViewHolder;
import com.gts.mycba.holder.TxnViewHolder;
import com.gts.mycba.model.CbaTxn;
import com.gts.mycba.model.TxnAtmData;
import com.gts.mycba.model.TxnData;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.LinkedList;

import static com.gts.mycba.CbaConstants.PENDING_PREFIX;

public class TxnRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context = null ;

    private TxnData txnData = null ;
    private LinkedList<CbaTxn> txns;
    ArrayList<TxnAtmData> atms;

    LayoutInflater inflater;
    DecimalFormat currencyFormatter = (DecimalFormat) NumberFormat.getCurrencyInstance();

    @IntDef({TYPE.SUMMARY, TYPE.HEADER_DATE, TYPE.TXN, TYPE.ATM_TXN })
    @Retention(RetentionPolicy.SOURCE)
    @interface TYPE {
        int SUMMARY = 0;
        int HEADER_DATE = 1;
        int TXN = 2;
        int ATM_TXN = 3;
    }

    public TxnRecyclerAdapter(Context context, TxnData txnData, LinkedList<CbaTxn> txns) {
        this.context = context;
        this.txns = txns;
        this.txnData = txnData;
        this.atms = txnData.atms;

        currencyFormatter.setNegativePrefix("-" + "$");
        currencyFormatter.setNegativeSuffix("");

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getItemViewType(int position) {
        if (position == TYPE.SUMMARY) {
            return TYPE.SUMMARY;
        } else {
            CbaTxn txn = getItem(position);

            if (CbaConstants.DATE_HEADER.equals(txn.id)) {
                return TYPE.HEADER_DATE;
            } else if (txn.atmId != null && txn.atmId.length() > 0) {
                return TYPE.ATM_TXN;
            } else {
                return TYPE.TXN;
            }
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE.SUMMARY:
                return new TxnHeaderViewHolder(inflater.inflate(R.layout.txn_header, parent, false));
            case TYPE.HEADER_DATE:
                return new TxnDateViewHolder(inflater.inflate(R.layout.time_header, parent, false));
            case TYPE.TXN:
                return new TxnViewHolder(context, inflater.inflate(R.layout.txn_item, parent, false));
            case TYPE.ATM_TXN:
                TxnAtmViewHolder txnAtmViewHolder = new TxnAtmViewHolder(context, inflater.inflate(R.layout.txn_item, parent, false));
                txnAtmViewHolder.atms = atms; //atm info to refer if the atm item is clicked
                return txnAtmViewHolder;
            default:
                return null;
        }
    }

    public CbaTxn getItem(int position) {
        return txns.get(position);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        CbaTxn txn = getItem(position);

        switch (getItemViewType(position)) {
            case TYPE.SUMMARY:
                TxnHeaderViewHolder headerHolder = (TxnHeaderViewHolder) viewHolder;
                headerHolder.accName.setText(txnData.account.accountName);
                headerHolder.accNumber.setText(txnData.account.accountNumber);
                headerHolder.availableAmt.setText(currencyFormatter.format(txnData.account.available));
                headerHolder.balanceAmt.setText(currencyFormatter.format(txnData.account.balance));
                break;

            case TYPE.HEADER_DATE:
                TxnDateViewHolder dateHolder = (TxnDateViewHolder) viewHolder;
                dateHolder.date.setText(txn.effectiveDate);
                dateHolder.dateDesc.setText(txn.description);
                break;

            case TYPE.ATM_TXN:
                TxnViewHolder txnAtmHolder = (TxnViewHolder) viewHolder;
                ((TxnAtmViewHolder) txnAtmHolder).txn = txn;
                txnAtmHolder.itemDesc.setText(Html.fromHtml(txn.description));
                txnAtmHolder.itemAmt.setText(currencyFormatter.format(txn.amount));
                txnAtmHolder.atmIcon.setVisibility(View.VISIBLE);
                break;

            case TYPE.TXN:
                TxnViewHolder txnHolder = (TxnViewHolder) viewHolder;

                SpannableString ss = new SpannableString(Html.fromHtml(txn.description));
                if (txn.description.startsWith(PENDING_PREFIX)) {
				    ss.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), 0, 8, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

			    }
                txnHolder.itemDesc.setText(ss);
                txnHolder.itemAmt.setText(currencyFormatter.format(txn.amount));
                txnHolder.atmIcon.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return txns.size();
    }
}
