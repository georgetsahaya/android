package com.gts.mycba.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.gts.mycba.R;

public class TxnDateViewHolder extends RecyclerView.ViewHolder {
    public TextView date, dateDesc;

    public TxnDateViewHolder(View view) {
        super(view);

        date = (TextView) view.findViewById(R.id.item_date);
        dateDesc = (TextView) view.findViewById(R.id.item_date_desc);
    }
}
