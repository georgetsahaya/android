package com.gts.mycba.holder;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.gts.mycba.MapsActivity;
import com.gts.mycba.R;
import com.gts.mycba.model.CbaTxn;
import com.gts.mycba.model.TxnAtmData;

import java.util.ArrayList;
import java.util.LinkedList;

public class TxnViewHolder extends RecyclerView.ViewHolder {
    public TextView itemDesc, itemAmt;
    public ImageView atmIcon;
    public Context context;

    public TxnViewHolder(Context context, View view) {
        super(view);
        this.context = context;


        itemDesc = (TextView) view.findViewById(R.id.item_desc);
        itemAmt = (TextView) view.findViewById(R.id.item_amt);
        atmIcon = (ImageView) view.findViewById(R.id.atm_icon);
    }


}
