package com.gts.mycba.holder;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.gts.mycba.MapsActivity;
import com.gts.mycba.model.CbaTxn;
import com.gts.mycba.model.TxnAtmData;
import com.gts.mycba.model.TxnAtmLocation;

import java.util.ArrayList;

import static com.gts.mycba.MapsActivity.LAT;
import static com.gts.mycba.MapsActivity.LNG;

public class TxnAtmViewHolder extends TxnViewHolder implements View.OnClickListener {
    public CbaTxn txn;
    public ArrayList<TxnAtmData> atms;

    public TxnAtmViewHolder(Context context, View view) {
        super(context, view);

        view.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(context, MapsActivity.class);
        intent.putExtra(LAT, getAtmData().lat);
        intent.putExtra(LNG, getAtmData().lng);
        context.startActivity(intent);
    }

    public TxnAtmLocation getAtmData() {
        for (TxnAtmData data:atms) {
            if (data.id.equals(txn.atmId) && data.location != null) {
                return data.location;
            }
        }

        return new TxnAtmLocation();
    }


}
