package com.gts.mycba.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.gts.mycba.R;

public class TxnHeaderViewHolder extends RecyclerView.ViewHolder {
    public TextView accName, accNumber, availableAmt, balanceAmt;

    public TxnHeaderViewHolder(View view) {
        super(view);

        accName = (TextView) view.findViewById(R.id.account_name);
        accNumber = (TextView) view.findViewById(R.id.acc_no);
        availableAmt = (TextView) view.findViewById(R.id.available_amt);
        balanceAmt = (TextView) view.findViewById(R.id.balance_amt);

    }
}
