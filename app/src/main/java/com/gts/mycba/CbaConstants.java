package com.gts.mycba;

public interface CbaConstants {
    String URL = "https://www.dropbox.com/s/tewg9b71x0wrou9/data.json?dl=1";
    String JSONCONTENTFILE = "exercise.json" ;
    String DATEFLAG = "DATE";
    String DATE_HEADER = "HEADERDATE";
    String DATE_FORMAT = "dd/MM/yyyy";
    long MSEC_IN_A_DAY = 1000 * 60 * 60 * 24;
    String PENDING_PREFIX = "PENDING: ";
    int ZOOM_LEVEL = 10;
}
