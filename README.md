https://georgetsahaya@bitbucket.org/georgetsahaya/android.git

This sample application focuses to accomplish the requirements mentioned in the brief.txt provided as an exercise.

As by the requirement the code is provided as a zip file and the app apk is attached in the mail.

The list of requirement and the respective status in terms of code are given below;

1) Requirement: show a list of transactions corresponding to an account (Android.Transactions.png)
[Status: Done]

2) Requirement: Example data has been included (exercise.json)
[Status: Done. Both the logics of parsing the carried inside the project assets folder list and the logic to download the url are there in the code. If the internet connectivity is there, the code downloads the json from the provided url, uses the local exercise.json otherwise ]

3) Requirement: Pending transactions are combined with the transactions.

Bonus requirements
—————————---------

4) Requirement: Group transactions by date
[Status: Done]

5) Requirement: Indicate ATM Withdrawals by icon
[Status: Done. The placement of icon is on the bottom right corner of the list as the VD is not provided for this case.]

6) Requirement: Tapping on an ATM withdrawal row will show the location of the ATM on a map
[Status: Done]

7) Requirement: Implement an algorithm that will give the user a projected spend over the next two weeks
[Status: Done. There is a option menu available in the top right corner of the MainActivity action bar. Clicking on this menu calculates and provides the average fortnight spending. The wordings is not to show for the next weeks as the transactions in the json provided are very old.]

8) Requirement: Dynamically pull down data from https://www.dropbox.com/s/tewg9b71x0wrou9/data.json?dl=1
[Status: Done. The data is pulled from this url if there is internet connection, uses the local json data carried otherwise]

There is no focus on the unit testing as it is not part of the requirement, but if required I can add them.
